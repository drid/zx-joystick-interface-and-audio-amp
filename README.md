View this project on [CADLAB.io](https://cadlab.io/project/26909). 

# Sinclair ZX Spectrum Joystick Interface

This project is a Kempston Joystick interface for the ZX Spectrum 48K/128K  
A set of leds is connected to each axis for connection debugging

As an extra, an audio amplifier is included for amplifying 1Vpp audio signals of modern audio devices to levels that the ZX Spectrum can detect  
A VU meter displays the audio signal levels and helps adjust volume

The design uses only through hole components to preserve the feel of the era.

## Construction
Although there are no obsolete (at the time) components used in this design the ZX Connector will need to be trimmed on its sides to fit.  
The 5th column of pins can be removed and replaced by a small piece of plastic or PCB to act as an orientation key.

A trimmer on the back side is used to adjust the amplification level. Set the volume of your playback device to 70% and adjust the trimmer until you get all green bars (7 bars)  

![zx-joystick-front](zx-joystick-front.png)
![zx-joystick-back](zx-joystick-back.png)
